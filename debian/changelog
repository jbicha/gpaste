gpaste (3.42.2-1) unstable; urgency=medium

  * New upstream version 3.42.2

 -- Jérémy Lal <kapouer@melix.org>  Thu, 09 Dec 2021 15:47:41 +0100

gpaste (3.42.1-1) unstable; urgency=medium

  * New upstream version 3.42.1
  * Standards-Version 4.6.0.1

 -- Jérémy Lal <kapouer@melix.org>  Wed, 03 Nov 2021 19:05:41 +0100

gpaste (3.42.0-4) unstable; urgency=medium

  * Extension should Depends: gnome-shell. Closes: #996700

 -- Jérémy Lal <kapouer@melix.org>  Sun, 17 Oct 2021 19:23:36 +0200

gpaste (3.42.0-3) unstable; urgency=medium

  * Fix depends gnome-shell 41 << 42

 -- Jérémy Lal <kapouer@melix.org>  Sun, 17 Oct 2021 18:02:13 +0200

gpaste (3.42.0-2) unstable; urgency=medium

  * Upload to unstable (Closes: #996058). 

 -- Jérémy Lal <kapouer@melix.org>  Sun, 17 Oct 2021 11:45:37 +0200

gpaste (3.42.0-1) experimental; urgency=medium

  * New upstream version 3.42.0
  * Depends: gnome-shell 3.41 (Closes: #996058).

 -- Jérémy Lal <kapouer@melix.org>  Sun, 10 Oct 2021 22:39:41 +0200

gpaste (3.40.2-1) unstable; urgency=medium

  * New upstream version 3.40.2 (Closes:#993064)
  * Update symbols - ui methods removed on purpose

 -- Jérémy Lal <kapouer@melix.org>  Fri, 27 Aug 2021 10:53:03 +0200

gpaste (3.38.6-1) unstable; urgency=medium

  * New upstream version 3.38.6 (Closes:#984943).

 -- Jérémy Lal <kapouer@melix.org>  Wed, 10 Mar 2021 19:48:13 +0100

gpaste (3.38.5-1) unstable; urgency=medium

  * New upstream version 3.38.5
  * Standards-Version 4.5.1, no change required

 -- Jérémy Lal <kapouer@melix.org>  Sun, 14 Feb 2021 23:28:01 +0100

gpaste (3.38.4-1) unstable; urgency=medium

  * New upstream version 3.38.4

 -- Jérémy Lal <kapouer@melix.org>  Mon, 04 Jan 2021 10:42:21 +0100

gpaste (3.38.3-2) unstable; urgency=medium

  * Fix version in Breaks/Replaces. Closes: #975890

 -- Jérémy Lal <kapouer@melix.org>  Thu, 26 Nov 2020 12:10:04 +0100

gpaste (3.38.3-1) unstable; urgency=medium

  * New upstream version 3.38.3
  * Rename to gnome-shell-extension-gpaste, closes: #975473
  * watch file version 4, use example from man uscan

 -- Jérémy Lal <kapouer@melix.org>  Sun, 22 Nov 2020 18:55:06 +0100

gpaste (3.38.2-1) unstable; urgency=medium

  * New upstream version 3.38.2 (Closes: #972038)
  * Refresh patch

 -- Jérémy Lal <kapouer@melix.org>  Sun, 18 Oct 2020 13:58:32 +0200

gpaste (3.36.3-1) unstable; urgency=medium

  * New upstream version 3.36.3 (Closes: #955132)

 -- Jérémy Lal <kapouer@melix.org>  Fri, 27 Mar 2020 19:34:36 +0100

gpaste (3.36.2-1) unstable; urgency=medium

  * New upstream version 3.36.2 (Closes: #954995)

 -- Jérémy Lal <kapouer@melix.org>  Thu, 26 Mar 2020 14:18:33 +0100

gpaste (3.36.0-1) unstable; urgency=medium

  * New upstream version 3.36.0

 -- Jérémy Lal <kapouer@melix.org>  Thu, 12 Mar 2020 23:10:53 +0100

gpaste (3.34.1-2) unstable; urgency=medium

  [ Laurent Bigonville ]
  * Bump debhelper compatibility to 12 and move the daemon to /usr/libexec
  * debian/gpaste.install: Install bash and zsh completion for gpaste-client
  * debian/control: Bump Standards-Version to 4.5.0 (no further changes)

  [ Jérémy Lal ]
  * Drop unnecessary build-dep on mutter. Closes: #953004

 -- Jérémy Lal <kapouer@melix.org>  Tue, 03 Mar 2020 10:57:16 +0100

gpaste (3.34.1-1) unstable; urgency=medium

  [ Jérémy Lal ]
  * New upstream version 3.34.1 (Closes: #941729)

  [ Jeremy Bicha ]
  * Set $XDG_RUNTIME_DIR to clean up some build log noise

 -- Jérémy Lal <kapouer@melix.org>  Thu, 10 Oct 2019 19:22:20 +0200

gpaste (3.34.0-1) experimental; urgency=medium

  * New upstream version 3.34.0
  * Update symbols file
  * Build-Depends libmutter-5-dev

 -- Jérémy Lal <kapouer@melix.org>  Thu, 12 Sep 2019 01:17:03 +0200

gpaste (3.32.0-2) unstable; urgency=medium

  * Release to unstable 

 -- Jérémy Lal <kapouer@melix.org>  Thu, 12 Sep 2019 00:43:30 +0200

gpaste (3.32.0-1) experimental; urgency=medium

  * New upstream version 3.32.0
  * Fix Vcs-Git url
  * Depends libmutter-4-dev
  * Fix typo in description
  * Standards-Version 4.3.0

 -- Jérémy Lal <kapouer@melix.org>  Tue, 30 Apr 2019 10:50:40 +0200

gpaste (3.30.2-1) unstable; urgency=medium

  * New upstream version 3.30.2
  * Update symbols
  * Expose Build-Depends-Package in symbols file

 -- Jérémy Lal <kapouer@melix.org>  Wed, 12 Dec 2018 18:17:20 +0100

gpaste (3.30.1-2) unstable; urgency=medium

  * Release to unstable 

 -- Jérémy Lal <kapouer@melix.org>  Sat, 03 Nov 2018 22:33:41 +0100

gpaste (3.30.1-1) experimental; urgency=medium

  * New upstream version 3.30.1
  * Remove upstream patches
  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field

 -- Jérémy Lal <kapouer@melix.org>  Wed, 03 Oct 2018 14:46:52 +0200

gpaste (3.28.2-1) unstable; urgency=medium

  * New upstream version 3.28.2
  * Cherry-pick upstream patches for gnome 3.30
  * Build-Depends mutter-3-dev (Closes: #908127)
  * Depends glib2.0 >= 2.58.0
  * Bump SONAME from 9 to 11, update symbols

 -- Jérémy Lal <kapouer@melix.org>  Thu, 06 Sep 2018 15:29:43 +0200

gpaste (3.28.0-2) unstable; urgency=medium

  * Upload to unstable:
    - glib 2.56 is available
    - appstream fixed by 0.7.7-2

 -- Jérémy Lal <kapouer@melix.org>  Sat, 17 Mar 2018 21:50:39 +0100

gpaste (3.28.0-1) experimental; urgency=medium

  * New upstream version 3.28.0
  * Remove patch applied upstream
  * Depends glib2.0 >= 2.56.0
  * Build-Conflicts pkgconfig (<< 1.3), see #893079.

 -- Jérémy Lal <kapouer@melix.org>  Fri, 16 Mar 2018 09:50:37 +0100

gpaste (3.26.0-5) unstable; urgency=high

  * Patch and update deps for mutter-2 (Closes: #892833).

 -- Jérémy Lal <kapouer@melix.org>  Tue, 13 Mar 2018 15:19:50 +0100

gpaste (3.26.0-4) unstable; urgency=medium

  [ Matthias Klumpp ]
  * Fix metainfo install location (Closes: #882002)

 -- Jérémy Lal <kapouer@melix.org>  Fri, 17 Nov 2017 15:32:21 +0100

gpaste (3.26.0-3) unstable; urgency=medium

  * Drop systemd dependency, use --with-systemduserunitdir instead
    (Closes: #880418)

 -- Jérémy Lal <kapouer@melix.org>  Tue, 31 Oct 2017 12:39:55 +0100

gpaste (3.26.0-2) unstable; urgency=medium

  * Use dh --with gir, Closes: #880417

 -- Jérémy Lal <kapouer@melix.org>  Tue, 31 Oct 2017 12:17:18 +0100

gpaste (3.26.0-1) unstable; urgency=medium

  * New upstream version 3.26.0
  * pkgconf implements PKG_PREREQ as of version 1.3
  * Release to unstable (Closes: #880083)

 -- Jérémy Lal <kapouer@melix.org>  Sun, 29 Oct 2017 14:21:30 +0100

gpaste (3.24.2-1) experimental; urgency=medium

  [ Jeremy Bicha ]
  * New upstream version 3.24.2
  * Fix package naming
  * Drop gpaste applet, no longer provided in this source package
  * Bump required gnome-shell version for extension to 3.24
  * Build with mutter 3.25
  * Switch from cdbs to dh
  * Bump debhelper compat to 10 (Closes: #824655)
  * Update Vcs-Browser link

  [ Jérémy Lal ]
  * Upload to experimental (Closes: #874168)
  * Tighten dependency on pkg-config for PKG_PREREQ is used
  * Useless autotools-dev depends since debhelper 10

 -- Jérémy Lal <kapouer@melix.org>  Mon, 04 Sep 2017 00:21:24 +0200

gpaste (3.22.4-1) unstable; urgency=medium

  * New upstream version 3.22.4

 -- Jérémy Lal <kapouer@melix.org>  Tue, 27 Jun 2017 08:41:27 +0200

gpaste (3.22.0-1) unstable; urgency=medium

  * New upstream version 3.22.0
  * Harden build

 -- Jérémy Lal <kapouer@melix.org>  Sun, 09 Oct 2016 21:57:48 +0200

gpaste (3.21.91-1) unstable; urgency=medium

  * New upstream version 3.21.91

 -- Jérémy Lal <kapouer@melix.org>  Sat, 17 Sep 2016 16:03:37 +0200

gpaste (3.21.90-1) unstable; urgency=medium

  * New upstream version 3.21.90
  * Build-Depends libmutter-dev

 -- Jérémy Lal <kapouer@melix.org>  Sat, 10 Sep 2016 00:23:02 +0200

gpaste (3.20.4-1) unstable; urgency=medium

  * Imported Upstream version 3.20.4

 -- Jérémy Lal <kapouer@melix.org>  Mon, 18 Jul 2016 21:11:35 +0200

gpaste (3.20.3-1) unstable; urgency=medium

  * Imported Upstream version 3.20.3
  * Remove applied patches
  * libgpaste6 should be in Section libs. Closes: #825356.
  * Standards-Version 3.9.8

 -- Jérémy Lal <kapouer@melix.org>  Fri, 17 Jun 2016 09:05:15 +0200

gpaste (3.20-2) unstable; urgency=medium

  * Install systemd user units (Closes: #822314)
  * Build-Depends systemd for systemduserunitdir variable

 -- Jérémy Lal <kapouer@melix.org>  Sun, 24 Apr 2016 01:47:41 +0200

gpaste (3.20-1) unstable; urgency=medium

  * Imported Upstream version 3.20
  * License switch from GPL-3 to BSD-2-clause
  * Build-Depends gtk 3.20, vala 0.32, gir 1.48
  * Build-Depends libappstream-glib-dev
  * Secure Vcs url
  * Standards-Version 3.9.7
  * Remove -dbg package
  * Library version bump from 4 to 6
  * Import upstream commits from 3.20 branch

 -- Jérémy Lal <kapouer@melix.org>  Sat, 02 Apr 2016 15:31:55 +0200

gpaste (3.18.3-1) unstable; urgency=medium

  * Imported Upstream version 3.18.3
  * Update symbols

 -- Jérémy Lal <kapouer@melix.org>  Wed, 20 Jan 2016 23:46:00 +0100

gpaste (3.18.2-1) unstable; urgency=medium

  * Imported Upstream version 3.18.2

 -- Jérémy Lal <kapouer@melix.org>  Sun, 18 Oct 2015 22:51:40 +0200

gpaste (3.18.1.1-1) unstable; urgency=medium

  * Imported Upstream version 3.18.1.1 (Closes: #800678)
  * Soname bump to 4
  * Install gnome-shell search provider
  * Renamed gpaste to gpaste-client
  * Drop patch for gnome-shell 3.17.x compatibility

 -- Jérémy Lal <kapouer@melix.org>  Fri, 02 Oct 2015 14:38:14 +0200

gpaste (3.16.3-2) unstable; urgency=medium

  * Patch: allow more gnome-shell versions

 -- Jérémy Lal <kapouer@melix.org>  Tue, 08 Sep 2015 12:23:19 +0200

gpaste (3.16.3-1) unstable; urgency=medium

  * Imported Upstream version 3.16.3

 -- Jérémy Lal <kapouer@melix.org>  Tue, 08 Sep 2015 12:12:12 +0200

gpaste (3.16.2.1-1) unstable; urgency=medium

  * Imported Upstream version 3.16.2.1 (Closes: #789255)
  * gpaste-applet does not need a manpage - upstream dropped it
  * Build-Depends gtk >= 3.16
  * Depends gnome-shell >= 3.16
  * Major API changes, bump soname to 3

 -- Jérémy Lal <kapouer@melix.org>  Fri, 19 Jun 2015 11:05:16 +0200

gpaste (3.14.4.1-2) unstable; urgency=medium

  * Build-Depends appstream-utils (Closes: #788360).

 -- Jérémy Lal <kapouer@melix.org>  Wed, 10 Jun 2015 19:51:57 +0200

gpaste (3.14.4.1-1) unstable; urgency=medium

  * Build libgpaste2-dbg package (Closes: #773945).

 -- Jérémy Lal <kapouer@melix.org>  Mon, 25 May 2015 19:59:42 +0200

gpaste (3.14-2) unstable; urgency=medium

  * Upstream patch fixing gnome-shell extension crashes.

 -- Jérémy Lal <kapouer@melix.org>  Sat, 07 Mar 2015 14:44:36 +0100

gpaste (3.14-1) unstable; urgency=medium

  * Imported Upstream version 3.14 (Closes: #764803).
  * Remove/unapply all patches, applied upstream
  * Update symbols

 -- Jérémy Lal <kapouer@melix.org>  Sat, 11 Oct 2014 12:18:43 +0200

gpaste (3.12.3.1-1) unstable; urgency=medium

  * Imported Upstream version 3.12.3.1
  * Standards-Version 3.9.6

 -- Jérémy Lal <kapouer@melix.org>  Thu, 09 Oct 2014 22:12:19 +0200

gpaste (3.12.3-2) unstable; urgency=medium

  * Provides/Conflicts with packages with soname 1.
    (Closes: #763367, #763366, #763362)

 -- Jérémy Lal <kapouer@melix.org>  Mon, 29 Sep 2014 21:55:26 +0200

gpaste (3.12.3-1) unstable; urgency=medium

  * Imported Upstream version 3.12.3
  * Patches:
    + Unapply missing_ldadd.patch, applied upstream
    + Fix missing dash in applet man page header
    + Extension add support for gnome-shell 3.14
  * Package gpaste-applet (Closes: #762521)
  * Bump soname to version 2.0, update symbols and
    packages names.

 -- Jérémy Lal <kapouer@melix.org>  Sat, 27 Sep 2014 15:39:51 +0200

gpaste (3.12.2-1) unstable; urgency=medium

  * Initial release (Closes: #647263)
    Thanks to Alin Andrei <webupd8@gmail.com>,
    Victor Seva <linuxmaniac@torreviejawireless.org>,
    Zang MingJie <zealot0630@gmail.com>,
    Leo Iannacone <l3on@ubuntu.com>.

 -- Jérémy Lal <kapouer@melix.org>  Tue, 29 Jul 2014 14:12:23 +0200
